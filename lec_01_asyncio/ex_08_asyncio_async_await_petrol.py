import asyncio
import random
import uuid


async def process_new_client(client_uuid):
    print(f"Plugging the hose into the gas station for client {client_uuid}")
    await asyncio.sleep(random.randint(0, 5))
    print(f"Getting money from client {client_uuid}")


def main():
    loop = asyncio.get_event_loop()

    for i in range(5):
        client_uuid = str(uuid.uuid4())
        loop.create_task(process_new_client(client_uuid))

    loop.run_forever()


if __name__ == '__main__':
    main()
