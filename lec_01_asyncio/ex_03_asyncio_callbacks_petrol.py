import asyncio
import functools
import random
import uuid


def get_money(client_uuid):
    print(f"Getting money from client {client_uuid}")


def process_new_client(client_uuid):
    print(f"Plugging the hose into the gas station for client {client_uuid}")
    asyncio.get_event_loop().call_later(random.randint(0, 5), functools.partial(get_money, client_uuid))


def main():
    loop = asyncio.get_event_loop()

    for i in range(5):
        client_uuid = str(uuid.uuid4())
        loop.call_soon(functools.partial(process_new_client, client_uuid))

    loop.run_forever()


if __name__ == '__main__':
    main()
