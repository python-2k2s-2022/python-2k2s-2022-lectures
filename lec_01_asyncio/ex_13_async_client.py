import asyncio


async def tcp_echo_client(message):
    reader, writer = await asyncio.open_connection(
        '127.0.0.1', 8889)

    print(f'Send: {message!r}')
    writer.write(message.encode())
    await writer.drain()

    data = await reader.readline()
    print(f'Received: {data.decode()!r}')

    print('Close the connection')
    writer.close()


async def main():
    await asyncio.gather(*[tcp_echo_client(f'Hello World from {i}\n') for i in range(10)])

if __name__ == '__main__':
    asyncio.run(main())
