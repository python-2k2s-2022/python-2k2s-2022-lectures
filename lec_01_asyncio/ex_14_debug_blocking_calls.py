import asyncio
import time


async def blocking_func(param):
    print(f'Before blocking {param}')
    time.sleep(5)
    print(f'After blocking {param}')


async def main():
    await asyncio.gather(blocking_func(1), blocking_func(2))


if __name__ == '__main__':
    asyncio.run(main(), debug=True)
