import collections
import datetime
import functools
import random
import uuid


def get_money(client_uuid):
    print(f"Getting money from client {client_uuid}")


def process_new_client(client_uuid):
    print(f"Plugging the hose into the gas station for client {client_uuid}")
    return functools.partial(get_money, client_uuid), random.randint(0, 5)


class EventLoop:
    def __init__(self):
        self._ready_tasks = collections.deque()
        self._pending_tasks = list()

    def call_soon(self, func):
        self._ready_tasks.append(func)

    def call_later(self, func, delay):
        self._pending_tasks.append(
            (func, datetime.datetime.now() + datetime.timedelta(seconds=delay))
        )

    def _run_once(self):
        current_dt = datetime.datetime.now()
        self._ready_tasks.extend(
            map(
                lambda el: el[0],
                filter(lambda el: current_dt > el[1], self._pending_tasks)
            )
        )
        self._pending_tasks = list(filter(lambda el: current_dt <= el[1], self._pending_tasks))

        ready_tasks_count = len(self._ready_tasks)
        for _ in range(ready_tasks_count):
            task = self._ready_tasks.popleft()
            result = task()
            if result is not None:
                func, seconds = result
                if seconds == 0:
                    self.call_soon(func)
                else:
                    self.call_later(func, seconds)

    def run_forever(self):
        while True:
            self._run_once()


def main():
    loop = EventLoop()

    for i in range(5):
        client_uuid = str(uuid.uuid4())
        loop.call_soon(functools.partial(process_new_client, client_uuid))

    loop.run_forever()


if __name__ == '__main__':
    main()
