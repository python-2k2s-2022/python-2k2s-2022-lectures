import asyncio


async def waiting_coro(text):
    print(f'Before waiting 5 seconds after {text}')
    await asyncio.sleep(5)
    print(f'After waiting 5 seconds after {text}')


async def create_task_example():
    print('Before create task')
    asyncio.create_task(waiting_coro('create_task_example'))
    print('After create task')


async def await_example():
    print('Before await')
    await waiting_coro('await_example')
    print('After await')


async def main():
    await create_task_example()
    await await_example()


if __name__ == '__main__':
    asyncio.run(main())
