from typing import Protocol


class A(Protocol):
    def super_method(self):
        pass


class B:
    def super_method(self):
        pass


def func(a: A):
    pass


def main() -> None:
    # func(A())  # error: Cannot instantiate protocol class "A"
    func(B())


if __name__ == '__main__':
    main()
