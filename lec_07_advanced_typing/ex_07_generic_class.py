from __future__ import annotations
from typing import TypeVar, Generic


T = TypeVar('T')


class LinkedList(Generic[T]):
    def __init__(self, first_element: T, tail: LinkedList[T] = None) -> None:
        self.head = first_element
        self.tail = tail

    def add(self, next_element: T) -> LinkedList[T]:
        return LinkedList(next_element, tail=self)


def main() -> None:
    a = LinkedList(5)
    a.add(6)
    a.add(7)
    a.add('str')


if __name__ == '__main__':
    main()
