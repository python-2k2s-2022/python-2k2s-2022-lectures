class Main {
    public static void main(String[] args) {
        Animal[] objs = {new Cat(), new Dog()};
        for (Animal a : objs) {
            System.out.println(a.yell);
        }

        Object[] objs = {new Cat(), new Dog()};
        for (Object o : objs) {
            System.out.println(o.yell);
        }

        Object[] objs = {new Cat(), new Dog()};
        for (Object o : objs) {
            System.out.println(((Animal)o).yell());
        }

        Animal[] objs = {new Cat(), new Dog(), new Person()};
        for (Animal a : objs) {
            System.out.println(a.yell);
        }

        Object[] objs = {new Cat(), new Dog(), new Person()};
        for (Object o : objs) {
            System.out.println(o.yell);
        }

        Object[] objs = {new Cat(), new Dog(), new Person()};
        for (Object o : objs) {
            System.out.println(((Animal)o).yell());
        }
    }
}

interface Animal {
    String yell();
}

class Cat implements Animal {
    @Override
    public String yell() {
        return "Meow!";
    }
}

class Dog implements Animal {
    @Override
    public String yell() {
        return "Bark!";
    }
}

class Person {
    public String yell() {
        return "I need money!";
    }
}
