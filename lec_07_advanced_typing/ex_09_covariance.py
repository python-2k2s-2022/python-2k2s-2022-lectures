from __future__ import annotations
from typing import TypeVar, Generic


T = TypeVar('T', covariant=True)


class ImmutablePair(Generic[T]):
    def __init__(self, first_element: T, second_element: T):
        self.first_element = first_element
        self.second_element = second_element


class Person:
    pass


class Student(Person):
    pass


def process_people(lst: ImmutablePair[Person]):
    pass


def main() -> None:
    a = ImmutablePair(Person(), Person())
    b = ImmutablePair(Student(), Student())
    process_people(a)
    process_people(b)


if __name__ == '__main__':
    main()
