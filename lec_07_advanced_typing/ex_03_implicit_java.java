class Main {
    public static void main(String[] args) {
        var x = 10;
        var y = 25;

        var z = x + y;

        System.out.println("Sum of x+y = " + z);
    }
}
