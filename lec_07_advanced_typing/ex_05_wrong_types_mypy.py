from typing import List


def check_existence(el: int, lst: List[int]):
    for i in lst:
        if i == el:
            return True
    return False


def main() -> None:
    print(check_existence('111', ['111', '3333']))


if __name__ == '__main__':
    main()
