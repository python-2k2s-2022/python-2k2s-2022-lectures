from typing import List


class Employee:
    pass


class Manager(Employee):
    pass


def add_employee(lst: List[Employee]):
    lst.append(Employee())


def main() -> None:
    managers_team: List[Manager] = [Manager(), Manager()]
    add_employee(managers_team)  # managers_team is not List[Manager] anymore


if __name__ == '__main__':
    main()
