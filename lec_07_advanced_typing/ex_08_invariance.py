from __future__ import annotations
from typing import TypeVar, Generic


T = TypeVar('T')


class LinkedList(Generic[T]):
    def __init__(self, first_element: T, tail: LinkedList[T] = None) -> None:
        self.head = first_element
        self.tail = tail

    def add(self, next_element: T) -> LinkedList[T]:
        return LinkedList(next_element, tail=self)


class Person:
    pass


class Student(Person):
    pass


def process_people(lst: LinkedList[Person]):
    pass


def main() -> None:
    b = LinkedList(Student())
    b.add(Student())
    process_people(b)


if __name__ == '__main__':
    main()
