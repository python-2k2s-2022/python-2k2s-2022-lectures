from typing import Protocol, runtime_checkable


@runtime_checkable
class A(Protocol):
    def super_method(self):
        pass


class B:
    def super_method(self):
        pass


def func(a: A):
    pass


def main() -> None:
    print(issubclass(B, A))
    print(isinstance(B(), A))


if __name__ == '__main__':
    main()
