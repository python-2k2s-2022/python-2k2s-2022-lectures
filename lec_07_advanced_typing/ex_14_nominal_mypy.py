class A:
    pass


class B(A):
    pass


def func(a: A):
    pass


def main() -> None:
    func(A())
    func(B())


if __name__ == '__main__':
    main()
