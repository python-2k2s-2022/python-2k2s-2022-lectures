from typing import List, TypeVar


T = TypeVar('T')


def check_existence(el: T, lst: List[T]) -> bool:
    for i in lst:
        if i == el:
            return True
    return False


def main() -> None:
    print(check_existence('111', ['111', '3333']))


if __name__ == '__main__':
    main()
