import java.util.*;

class Main {
    public static void main(String[] args) {
        System.out.println(checkExistence(5, new int[]{1, 2, 3, 5}));
        System.out.println(checkExistence(5, new int[]{1, 2, 3}));

        System.out.println(checkExistence(5.1, new double[]{1.1, 2.1, 3.1, 5.1}));
        System.out.println(checkExistence(5.1, new double[]{1.1, 2.1, 3.1}));

        List lst = new ArrayList();
        lst.add((Object)Integer.valueOf(5));
        lst.add((Object)Integer.valueOf(1));
        System.out.println(checkExistenceObj((Object)Integer.valueOf(5), lst));

        List<Integer> lst = new ArrayList<>();
        lst.add(Integer.valueOf(5));
        lst.add(Integer.valueOf(1));
        System.out.println(checkExistence(Integer.valueOf(5), lst));
    }

    public static boolean checkExistence(int el, int[] arr) {
        for (var currentEl : arr) {
            if (currentEl == el) {
                return true;
            }
        }
        return false;
    }

    public static boolean checkExistence(double el, double[] arr) {
        for (var currentEl : arr) {
            if (currentEl == el) {
                return true;
            }
        }
        return false;
    }

    public static boolean checkExistenceObj(Object el, List arr) {
        for (var currentEl : arr) {
            if (currentEl.equals(el)) {
                return true;
            }
        }
        return false;
    }

    public static <T> boolean checkExistence(T el, List<T> arr) {
        for (var currentEl : arr) {
            if (currentEl.equals(el)) {
                return true;
            }
        }
        return false;
    }
}
