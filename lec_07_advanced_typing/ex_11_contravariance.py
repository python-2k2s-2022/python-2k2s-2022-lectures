from __future__ import annotations
from typing import TypeVar, Generic


T = TypeVar('T', contravariant=True)


class Zoo(Generic[T]):
    def __init__(self):
        self.animals = []

    def add_animal(self, animal: T):
        self.animals.append(animal)


class Animal:
    pass


class Cat(Animal):
    pass


def process_zoo(zoo: Zoo[Cat]):
    pass


def main() -> None:
    cat_zoo: Zoo[Cat] = Zoo()
    cat_zoo.add_animal(Cat())
    process_zoo(cat_zoo)

    animal_zoo: Zoo[Animal] = Zoo()
    animal_zoo.add_animal(Animal())
    process_zoo(animal_zoo)

    object_zoo: Zoo[object] = Zoo()
    object_zoo.add_animal("I'm an animal, believe me!")
    object_zoo.add_animal(5)
    process_zoo(object_zoo)


if __name__ == '__main__':
    main()
