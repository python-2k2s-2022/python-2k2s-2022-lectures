class A:
    pass


class B(A):
    pass


class C:
    def method(self, b: B) -> A:
        return A()


class D(C):
    def method(self, b: A) -> B:
        return B()
