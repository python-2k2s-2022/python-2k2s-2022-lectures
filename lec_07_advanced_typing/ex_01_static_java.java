class Main {
    public static void main(String[] args) {
        System.out.println(checkExistence(5, new int[]{1, 2, 3, 5}));
        System.out.println(checkExistence(5, new int[]{1, 2, 3}));

        System.out.println(checkExistence(5.1, new double[]{1.1, 2.1, 3.1, 5.1}));
        System.out.println(checkExistence(5.1, new double[]{1.1, 2.1, 3.1}));
    }

    public static boolean checkExistence(int el, int[] arr) {
        for (var currentEl : arr) {
            if (currentEl == el) {
                return true;
            }
        }
        return false;
    }

    public static boolean checkExistence(double el, double[] arr) {
        for (var currentEl : arr) {
            if (currentEl == el) {
                return true;
            }
        }
        return false;
    }
}
