class A:
    pass


class B(A):
    pass


class C:
    def method(self, a: A) -> A:
        return A()


class D(C):
    def method(self, b: B) -> B:
        return B()
