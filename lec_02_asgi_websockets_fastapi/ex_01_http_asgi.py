import logging
import os
from typing import Callable

"""
pip install "uvicorn[standard]"
uvicorn ex_01_http_asgi:app
Open in browser and postman locally using port 8000
"""

LOG_LEVEL = logging.getLevelName(os.environ.get("LOG_LEVEL", "DEBUG"))
logging.root.setLevel(LOG_LEVEL)


async def read_body(receive: Callable):
    """
    Read and return the entire body from an incoming ASGI message.
    """
    body = b''
    more_body = True

    while more_body:
        message = await receive()
        logging.debug(' ====== BODY PART ====== ')
        logging.debug(message)
        body += message.get('body', b'')
        more_body = message.get('more_body', False)

    return body


async def app(scope: dict, receive: Callable, send: Callable):
    logging.debug(' ====== SCOPE ====== ')
    logging.debug(scope)
    request_body = await read_body(receive)
    logging.debug(' ====== TOTAL BODY ====== ')
    logging.debug(request_body)

    response_status = 200

    if scope['path'] == '/':
        response_body = b'<h1>Index page</h1>'
    elif scope['path'] == '/hello':
        response_body = b'<h1>Hello page</h1>'
    else:
        response_body = b'<h1>404 page</h1>'
        response_status = 404

    await send({
        'type': 'http.response.start',
        'status': response_status,
        'headers': [
            [b'content-type', b'text/html']
        ]
    })
    await send({
        'type': 'http.response.body',
        'body': response_body
    })
