from __future__ import annotations

import functools
import logging
import os
from typing import Callable

"""
pip install "uvicorn[standard]"
uvicorn ex_03_websockets_asgi:Connection
Open in browser locally using port 8000 (many tabs)
"""

LOG_LEVEL = logging.getLevelName(os.environ.get("LOG_LEVEL", "DEBUG"))
logging.root.setLevel(LOG_LEVEL)

html = """
<!DOCTYPE html>
<html>
    <head>
        <title>Chat</title>
    </head>
    <body>
        <h1>WebSocket Chat</h1>
        <h2>Your ID: <span id="ws-id"></span></h2>
        <form action="" onsubmit="sendMessage(event)">
            <input type="text" id="messageText" autocomplete="off"/>
            <button>Send</button>
        </form>
        <ul id='messages'>
        </ul>
        <script>
            var client_id = Date.now()
            document.querySelector("#ws-id").textContent = client_id;
            var ws = new WebSocket(`ws://localhost:8000/ws/${client_id}`);
            ws.onmessage = function(event) {
                var messages = document.getElementById('messages')
                var message = document.createElement('li')
                var content = document.createTextNode(event.data)
                message.appendChild(content)
                messages.appendChild(message)
            };
            function sendMessage(event) {
                var input = document.getElementById("messageText")
                ws.send(input.value)
                input.value = ''
                event.preventDefault()
            }
        </script>
    </body>
</html>
"""


def singleton(class_):
    instances = {}

    @functools.wraps(class_)
    def getinstance(*args, **kwargs):
        if class_ not in instances:
            instances[class_] = class_(*args, **kwargs)
        return instances[class_]
    return getinstance


@singleton
class ActiveConnectionsStorage:
    def __init__(self):
        self.active_connections = {}

    async def broadcast(self, message: str):
        for connection in self.active_connections.values():
            await connection.send_personal_message(message)

    def add_connection(self, connection: Connection):
        self.active_connections[connection.client_id] = connection

    def remove_connection(self, connection: Connection):
        del self.active_connections[connection.client_id]


class Connection:
    def __init__(self, scope: dict):
        self._scope = scope
        self._receive = None
        self._send = None
        self._client_id = ''

    @property
    def client_id(self):
        return self._client_id

    async def __call__(self, receive: Callable, send: Callable):
        self._receive = receive
        self._send = send
        await self._process_request()

    async def _process_request(self):
        if self._scope['type'] == 'http':
            await self._process_http_connection()
        if self._scope['type'] == 'websocket':
            await self._process_websocket_connection()

    async def _process_http_connection(self):
        await self._send({
            'type': 'http.response.start',
            'status': 200,
            'headers': [
                [b'content-type', b'text/html']
            ]
        })
        await self._send({
            'type': 'http.response.body',
            'body': html.encode('utf-8')
        })

    async def _process_websocket_connection(self):
        self._client_id = self._scope['path'][4:]
        client_request = await self._receive()
        if client_request['type'] == 'websocket.connect':
            await self._accept()
            ActiveConnectionsStorage().add_connection(self)
            await self._process_client_messages_loop()

    async def _accept(self):
        await self._send({
            'type': 'websocket.accept'
        })

    async def _process_client_messages_loop(self):
        while True:
            client_request = await self._receive()
            if client_request['type'] == 'websocket.disconnect':
                ActiveConnectionsStorage().remove_connection(self)
                await ActiveConnectionsStorage().broadcast(f'Client #{self.client_id} left the chat')
                break
            if client_request['type'] == 'websocket.receive':
                client_data = client_request.get('text') or client_request.get('bytes').decode('utf-8')
                await self.send_personal_message(f"You wrote: {client_data}")
                await ActiveConnectionsStorage().broadcast(f'Client #{self.client_id} says: {client_data}')

    async def send_personal_message(self, message: str):
        await self._send({
            'type': 'websocket.send',
            'text': message
        })
